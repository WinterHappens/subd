﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_SUBD
{
    class Program
    {


        struct Client
        {
            public int ID;
            public string Name;
            public int Age;
            public char Gender;
            public double Weight;
            public DateTime FirstDay;
            public string Description;
        }

        #region System Methods
        static void ResizeArray(ref Client[] clients, int newLength)
        {
            int curLength = clients.Length;
            int copyLength;

            //int copyLength = newLength > curLength ? curLength : newLength;

            Client[] newClients = new Client[newLength];

            if (newLength > curLength)
            {
                copyLength = curLength;
            }
            else
            {
                copyLength = newLength;
            }

            for (int i = 0; i < copyLength; i++)
            {
                newClients[i] = clients[i];
            }

            clients = newClients;
        }

        static Client[] CreateEmptyClientArray()
        {
            return new Client[0];
        }
        #endregion

        #region CRUD Methods

        static void AddNewClient(ref Client[] clients, Client newClient)
        {
            ResizeArray(ref clients, clients.Length + 1);
            clients[clients.Length - 1] = newClient;
        }

        static void DeleteClientByID(ref Client[] clients, int id)
        {
            int indexDelete = GetIndexByID(clients, id);
            if (indexDelete == -1)
            {
                Console.WriteLine("Element not found");
                return;
            }

            Client[] newClients = new Client[clients.Length - 1];

            int newI = 0;

            for (int i = 0; i < clients.Length; i++)
            {
                if (i != indexDelete)
                {
                    newClients[newI] = clients[i];
                    newI++;
                }
            }

            clients = newClients;
        }

        static void ClearAllClients(ref Client[] clients)
        {
            clients = CreateEmptyClientArray();
        }

        static void UpdateClientByID(Client[] clients, Client client)
        {
            int indexUpdate = GetIndexByID(clients, client.ID);
            if (indexUpdate == -1)
            {
                Console.WriteLine("Element not found");
                return;
            }


            clients[indexUpdate] = client;
        }

        static void InsertClientIntoPosition(ref Client[] clients, int position, Client client)
        {
            if (clients.Length == 0)
            {
                Console.WriteLine("Insert is impossible. Array is empty.");
            }

            if (position < 1 || position > clients.Length)
            {
                Console.WriteLine("Insert is impossible. Position not found.");
            }

            int indexInsert = position - 1;

            Client[] newClients = new Client[clients.Length + 1];
            int oldI = 0;

            for (int i = 0; i < newClients.Length; i++)
            {
                if (i != indexInsert)
                {
                    newClients[i] = clients[oldI];
                    oldI++;
                }
                else
                {
                    newClients[i] = client;
                }
            }

            clients = newClients;
        }

        #endregion

        #region Tools Methods

        static int GetIndexByID(Client[] clients, int id)
        {
            for (int i = 0; i < clients.Length; i++)
            {
                if (clients[i].ID == id)
                {
                    return i;
                }
            }

            return -1;
        }

        static void PrintClient(Client client)
        {
            Console.WriteLine("{0, -5}{1, -15}{2, -9}{3, -5}{4, -8}{5, -13}{6}", client.ID, client.Name, client.Age, client.Gender, client.Weight, client.FirstDay.ToShortDateString(), client.Description);
        }

        static void PrintManyClients(Client[] clients)
        {
            Console.WriteLine("{0, -5}{1, -15}{2, -9}{3, -5}{4, -8}{5, -13}{6}", "ID", "Имя", "Возраст", "Пол", "Вес", "Дата записи", "Описание");

            if (clients.Length == 0)
            {
                Console.WriteLine("Array is empty.");
            }
            else
            {
                for (int i = 0; i < clients.Length; i++)
                {
                    PrintClient(clients[i]);
                }
            }
            Console.WriteLine("-------------------------");
        }

        static Client CreateClient(int clientID)
        {
            Client client;

            client.ID = clientID;

            Console.Write("Введите имя клиента: ");
            client.Name = Console.ReadLine();

            Console.Write("Введите возраст клиента: ");
            client.Age = int.Parse(Console.ReadLine());

            Console.Write("Введите пол клиента: ");
            client.Gender = char.Parse(Console.ReadLine());

            Console.Write("Введите вес клиента: ");
            client.Weight = double.Parse(Console.ReadLine());

            Console.Write("Введите дату регистрации клиента: ");
            client.FirstDay = DateTime.Parse(Console.ReadLine());

            Console.Write("Введите описание клиента: ");
            client.Description = Console.ReadLine();

            return client;
        }
        #endregion

        #region Retrieve Methods

        static void PrintClientByID(Client[] clients, int id)
        {
            int indexPrint = GetIndexByID(clients, id);

            if (indexPrint == -1)
            {
                Console.WriteLine("Print is impossible. Element not found.");
                return;
            }

            PrintClient(clients[indexPrint]);
        }

        #endregion

        #region Interface Methods

        static void PrintMenu()
        {
            Console.WriteLine("1. Add new client");
            Console.WriteLine("2. Delete client by ID");
            Console.WriteLine("3. Clear all clients");
            Console.WriteLine("4. Update client by ID");
            Console.WriteLine("5. Insert client into position");
            Console.WriteLine("6. Print client by ID");
            Console.WriteLine("0. Exit");
        }

        static int InputInt(string message)
        {
            bool inputResult;
            int number;

            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out number);
            } while (!inputResult);

            return number;
        }

        #endregion

        static void Main(string[] args)
        {
            int clientAutoId = 0;
            Client[] clients = CreateEmptyClientArray();

            bool runProgram = true;

            while (runProgram)
            {
                Console.Clear();
                PrintManyClients(clients);

                PrintMenu();

                int menuPoint = InputInt("Input menu point: ");

                switch (menuPoint)
                {
                    case 6:
                        {
                            int id = InputInt("Input ID for print: ");
                            PrintClientByID(clients, id);
                            Console.ReadKey();
                        }
                        break;
                    case 5:
                        {
                            int position = InputInt("Input position for insert: ");
                            clientAutoId++;
                            Client client = CreateClient(clientAutoId);
                            InsertClientIntoPosition(ref clients, position, client);
                        }
                        break;
                    case 4:
                        {
                            int id = InputInt("Input ID for update: ");
                            Client client = CreateClient(id);
                            UpdateClientByID(clients, client);
                        }
                        break;
                    case 3:
                        {
                            ClearAllClients(ref clients);
                        }
                        break;
                    case 2:
                        {
                            int id = InputInt("Input ID for delete: ");
                            DeleteClientByID(ref clients, id);
                        }
                        break;
                    case 1:
                        {
                            clientAutoId++;
                            Client client = CreateClient(clientAutoId);
                            AddNewClient(ref clients, client);
                            //continue;
                        }
                        break;

                    case 0:
                        Console.WriteLine("Program will be finish");
                        Console.ReadKey();

                        runProgram = false;
                        break;

                    default:
                        Console.WriteLine("Error");
                        Console.ReadKey();
                        break;
                }
            }

            //Console.ReadKey();
        }
    }
}
